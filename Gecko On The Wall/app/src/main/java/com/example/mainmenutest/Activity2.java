package com.example.mainmenutest;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Point;
import android.os.Bundle;
import android.os.Handler;
import android.view.Display;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.Timer;
import java.util.TimerTask;

public class Activity2 extends AppCompatActivity {

    private TextView scoreLabel;
    private TextView startLabel;
    private ImageView box;
    private ImageView orange;
    private ImageView pink;
    private ImageView black;

    private int frameHeight;
    private int boxSize;
    private int screenWidth;
    private int screenHeight;

    private int score = 0;

    private int boxY;
    private int orangeX;
    private int orangeY;
    private int pinkX;
    private int pinkY;
    private int blackX;
    private int blackY;

    private Handler handler = new Handler();
    private Timer timer = new Timer();

    private boolean action_flg = false;
    private boolean start_flg = false;

    //private ParallaxView parallaxView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        setContentView(R.layout.activity_2);

        scoreLabel = (TextView) findViewById(R.id.scoreLabel);
        startLabel = (TextView) findViewById(R.id.startLabel);
        box = (ImageView) findViewById(R.id.box);
        orange = (ImageView) findViewById(R.id.orange);
        pink = (ImageView) findViewById(R.id.pink);
        black = (ImageView) findViewById(R.id.black);

        WindowManager wm =  getWindowManager();
        Display disp = wm.getDefaultDisplay();
        Point size = new Point();
        disp.getSize(size);

        screenHeight = size.y;
        screenWidth = size.x;

        orange.setX(-80);
        orange.setY(-80);
        pink.setX(-80);
        pink.setY(-80);
        black.setX(-80);
        black.setY(-80);

        scoreLabel.setText("Score : 0");

    }

    public void changePos()
    {
        hit();

        orangeX -= 12;
        if(orangeX < 0)
        {
            orangeX = screenWidth + 20;
            orangeY = (int) Math.floor(Math.random() * (frameHeight - orange.getHeight()));
        }
        orange.setX(orangeX);
        orange.setY(orangeY);

        blackX -= 16;
        if (blackX < 0)
        {
            blackX = screenWidth + 10;
            blackY = (int) Math.floor(Math.random() * (frameHeight - black.getHeight()));
        }
        black.setX(blackX);
        black.setY(blackY);

        pinkX -= 20;
        if (pinkX < 0)
        {
            pinkX = screenWidth + 500;
            pinkY = (int) Math.floor(Math.random() * (frameHeight - pink.getHeight()));
        }
        pink.setX(pinkX);
        pink.setY(pinkY);

        if (action_flg == true)
        {
            boxY -= 20;
        }
        else
        {
            boxY += 20;
        }

        if (boxY < 0) boxY = 0;scoreLabel.setText("Score : " + score);

        if (boxY > frameHeight - boxSize) boxY = frameHeight - boxSize;
        box.setY(boxY);


    }

    public void hit()
    {
        int orangeXcenter = orangeX + orange.getWidth();
        int orangeYcenter = orangeY + orange.getHeight();

        if (0 <= orangeXcenter && orangeXcenter <= boxSize && boxY <= orangeYcenter && orangeYcenter <= boxY + boxSize)
        {
            score += 50;
            orangeX = -10;
        }

        int pinkXcenter = pinkX + pink.getWidth();
        int pinkYcenter = pinkY + pink.getHeight();
        if(0 <= pinkXcenter && pinkXcenter <= boxSize && boxY <= pinkYcenter && pinkYcenter <= boxY + boxSize)
        {
            score += 100;
            pinkX = -10;
        }

        int blackXcenter = blackX + black.getWidth();
        int blackYcenter = blackY + black.getHeight();

        if(0 <= blackXcenter && blackXcenter <= boxSize && boxY <= blackYcenter && blackYcenter <= boxY + boxSize)
        {
            timer.cancel();
            timer = null;

            Intent intent = new Intent(getApplicationContext(), Result.class);
            intent.putExtra("SCORE", score);
            startActivity(intent);
        }
    }

    public boolean onTouchEvent(MotionEvent me)
    {
        if (start_flg == false)
        {
            start_flg = true;

            FrameLayout frame = (FrameLayout) findViewById(R.id.frame);
            frameHeight = frame.getHeight();

            boxY = (int)box.getY();

            boxSize = box.getHeight();

            startLabel.setVisibility(View.GONE);

            timer.schedule(new TimerTask() {
                @Override
                public void run() {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            changePos();
                        }
                    });
                }
            }, 0, 18);
        }
        else
        {
            if (me.getAction() == MotionEvent.ACTION_DOWN)
            {
                action_flg = true;
            }
            else if (me.getAction() == MotionEvent.ACTION_UP)
            {
                action_flg = false;
            }
        }

        return true;
    }
}
