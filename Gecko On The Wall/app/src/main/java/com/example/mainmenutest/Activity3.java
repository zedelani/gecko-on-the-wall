package com.example.mainmenutest;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class Activity3 extends AppCompatActivity {

    //MediaPlayer sound =  MediaPlayer.create(Activity3.this,R.raw.sound);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_3);

        Button On = findViewById(R.id.On);
        Button Off = findViewById(R.id.Off);
        Button back = findViewById(R.id.button6);

        final MediaPlayer sound =  MediaPlayer.create(Activity3.this,R.raw.sound);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Back();
                finish();
            }
        });

        On.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!sound.isPlaying()){
                    sound.start();
                }
            }
        });

        Off.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (sound.isPlaying()){
                    sound.pause();
                }
            }
        });
    }

    public void Back(){
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }
}
